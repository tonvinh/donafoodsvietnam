<?php 
/**
* Template Name: Gioi Thieu Right Sidebar 
*/
get_header();?>

<section id="main" class="container">
    <div class="subtitle">
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <h2><?php the_title(); ?></h2>
            </div>    
            <div class="col-xs-6 col-sm-6">
                <?php themeum_breadcrumbs(); ?>
            </div>
        </div>
    </div>
    <div class="row">
   
        <div id="content" class="site-content col-md-8" role="main">
            <?php


            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    get_template_part( 'post-format/content', get_post_format() );
                endwhile;
           
            endif;

            ?>

        </div>

        <div id="sidebar" class="col-md-4" role="complementary">
            
            <div class="sidebar-inner">
                <aside class="widget-area">
                <?php if(is_page_template('gioithieu-right-sidebar.php')) : ?>
            <div id="rss-2" class="widget widget_rss">
                <h3 class="widget_title">Về công ty</h3>
                <?php
                    
                if($post->post_parent == 0) {
                    $post_id = $post->ID;
                } else {
                    $post_id = $post->post_parent;
                }
                
                    $args = array(
                        'post_type' => 'page',
                        'post_parent' => $post_id,
                        'post_status' => 'publish'
                    );                            
                    $data = get_posts($args);                   
                    if(!empty($data)):
                ?>
                <ul>
                    <?php foreach($data as $item): ?>
                        <li class="<?php echo ($item->ID == $post->ID)?'active': ''; ?>"><a href="<?php echo get_permalink($item->ID)?>"> <?php echo $item->post_title; ?></a></li>                   
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
        <?php  endif;?>
                    <?php dynamic_sidebar( 'sidebar' ); ?>
                </aside>
            </div>
        </div> <!-- #sidebar -->        

    </div> <!-- .row -->
</section> <!-- .container -->

<?php get_footer();