    <?php get_header();
global $themeum_options;
?>

    <section id="main" class="container">
        <div class="row">
            <div id="content" class="site-content col-md-8" role="main">

                <?php if ( have_posts() ) : ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php
                     
                        get_template_part( 'post-format/content', get_post_format() ); ?>

                        <?php
                            // don't-delete 
                            $count_post = get_post_meta( $post->ID, '_post_views_count', true);
                            
                            if( $count_post == ''){
                                $count_post = 0;
                                delete_post_meta( $post->ID, '_post_views_count');
                                add_post_meta( $post->ID, '_post_views_count', $count_post);
                            }
                            else
                            {
                                $count_post = (int)$count_post + 1;
                                update_post_meta( $post->ID, '_post_views_count', $count_post);
                                
                            }
                        ?>
                    <?php endwhile; ?>

                    
                <?php else: ?>
                    <?php get_template_part( 'post-format/content', 'none' ); ?>
                <?php endif; ?>
                                    
                <div class="clearfix"></div>
                <?php 
//                    $shortcode = get_post_meta($post->ID, 'tuyendung_form_shortcode', true);
//                    $title = get_post_meta($post->ID, 'tuyendung_form_title', true);
                    
                    $shortcode = '[contact-form-7 id="3540" title="Tuyển dụng"]';
                    $title = 'Gửi thông tin ứng tuyển';
                ?>
                
                <?php if(!empty($title)): ?>
                    <h2><?php echo $title; ?></h2>
                <?php endif; ?>
                <?php echo do_shortcode($shortcode)?>
            </div> <!-- #content -->


            <?php get_sidebar(); ?>
            <!-- #sidebar -->

        </div> <!-- .row -->
    </section> <!-- .container -->

<?php get_footer();