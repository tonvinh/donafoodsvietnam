<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php 
global $themeum_options;
global $woocommerce; 
?>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
	<?php 

	if(isset($themeum_options['favicon'])){ ?>
		<link rel="shortcut icon" href="<?php echo $themeum_options['favicon']['url']; ?>" type="image/x-icon"/>
	<?php }else{ ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/images/plus.png' ?>" type="image/x-icon"/>
	<?php } ?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
	<style type="text/css">
	input[type="submit"],
		button,
		.single-post .post-navigation,
		.widget .nav,
		.book_wrapper a.btn-download,
		.btn-common,
		.profile-tab ul.nav-tabs>li>a:hover,
		.profile-tab ul.nav-tabs>li.active>a,
		.profile-tab ul.nav-tabs>li.active>a:hover,
		.profile-tab ul.nav-tabs>li.active>a:focus,
		.pagination>.active>a, .pagination>.active>span,
		.pagination>.active>a:hover,
		.pagination>.active>span:hover,
		.pagination>.active>a:focus,
		.pagination>.active>span:focus,
		.pagination>li>a:hover,
		.pagination>li>span:hover,
		.pagination>li>a:focus,
		.pagination>li>span:focus,
		.single-post .post-navigation .post-controller .previous-post a,
		.single-post .post-navigation .post-controller .next-post a,
		.post-content.media .post-format i,
		#searchform .btn-search,
		#blog-gallery-slider .carousel-control.left,
		#blog-gallery-slider .carousel-control.right, 
		.featured-image .entry-date ,
		.entry-header .no-image,
		.format-no-image .entry-date,
		.woocommerce span.onsale,
		.woocommerce-page span.onsale,
		.product-thumbnail-outer .addtocart-btn,
		.woocommerce .quantity .minus, 
		.woocommerce-page .quantity .minus, 
		.woocommerce #content .quantity .minus, 
		.woocommerce-page #content .quantity .minus,
		.woocommerce .quantity .plus, 
		.woocommerce-page .quantity .plus, 
		.woocommerce #content .quantity .plus, 
		.woocommerce-page #content .quantity .plus,
		.woocommerce-tabs .nav-tabs>li.active>a, 
		.woocommerce-tabs .nav-tabs>li.active>a:hover, 
		.woocommerce-tabs .nav-tabs>li.active>a:focus,
		.woocommerce-tabs .nav>li>a:hover, 
		.woocommerce-tabs .nav>li>a:focus,
		.woocommerce .woocommerce-message,
		.woocommerce-page .woocommerce-message,
		.woocommerce .woocommerce-info,
		.woocommerce-page .woocommerce-info,
		.features-list,
		.feature-img-wrapper, 
		.map-content, 
		.pricing-plan .plan-price, 
		.page-template-homepage-php .header #main-menu .nav>li>a:before,
		.page-template-homepage-php .header #main-menu .nav>li.active>a:before,		
		#main-menu .nav>li>ul li a:before,
		#main-menu .nav>li>ul li.active a:before,
		.review-image-wrapper .comments a,
		.testimonial-carousel-control:hover,
		.themeum_button_shortcode:hover,
		.span-title2:before,
		.portfolio-filter li a:before,
		.thumb-overlay a,
		.portfolio-title:after,
		.box-content .box-btn:hover,
		.list.list-number-background li:before,
		.drop-cap:first-letter,
		.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav a:hover, 
		.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav .ui-tabs-active a,
		.subtitle h2::after,
		.tribe-events-page-title::after,
		.tribe-events-calendar th,
		.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
		#tribe-events .tribe-events-button, .tribe-events-button,
		.wpb_wrapper .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header.ui-accordion-header-active,
		.load-more,
		.product-thumbnail-outer:after, .product-thumbnail-outer:before, .product-thumbnail-outer-inner:after, .product-thumbnail-outer-inner:before,
		.btn-pricing, .calender-date,.home-search,#single-portfolio .previous-post a:hover, #single-portfolio .next-post a:hover,
		.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,
		.woocommerce-page .widget_price_filter .price_slider_wrapper .ui-widget-content { background-color: #62a83d; }.style-title1:after,.review-item-text .entry-title a,.testimonial-carousel-control,.box-content .box-btn:hover,
		.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav a:hover, 
		.wpb_content_element .wpb_tour_tabs_wrapper .wpb_tabs_nav .ui-tabs-active a,
		.widget h3.widget_title,
		.entry-title.blog-entry-title,
		.bottom-widget .widget h3.widget_title:after,.widget .tagcloud a:hover,.themeum_button_shortcode:hover,
		#single-portfolio .previous-post a, #single-portfolio .next-post a,.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
		.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle{ border-color: #62a83d !important; }.title-variation1 .style-title1{ background-color: rgba(98,168,61,.8); }a, #mobile-menu ul li:hover > a,
		#mobile-menu ul li.active > a, span.badge,
		.comingsoon .social-share ul li a:hover,
		#comingsoon-countdown .countdown-period,
		.widget .nav > li.active > a, .widget .nav > li:hover > a,
		.widget.widget_mc4wp_widget .button i,
		.navbar-main .dropdown-menu>li>a:hover,
		.navbar-main .dropdown-menu>li>a:focus,
		.widget.widget_mc4wp_widget h3:before,
		.widget .tagcloud a:hover,
		.widget caption, .widget thead th,
		.footer-menu >li >a:hover,
		.entry-link h4,
		.post-content.media h2.entry-title a:hover,
		.widget.widget_rss ul li a,
		.widget.widget_recent_comments ul li a,
		.subtitle h2,
		h2.tribe-events-page-title,
		.entry-qoute blockquote small,
		.format-link .entry-link h4,
		.comments-title,
		#respond .comment-reply-title,
		.comment-list .comment-body .comment-author,
		.entry-meta ul li a,
		.widget-blog-posts .entry-title a:hover,
		.widget ul li a:hover,
		.organic-testimonial .testimonial-desc i,
		.woocommerce .star-rating,
		#main-menu .sub-menu li.active > a,
		.testimonial-carousel-control,
		.list-circle ul li:before,
		.list-star ul li:before,
		.author-company,
		.themeum-feature-box.feature-box-shop,
		#main-menu .sub-menu li:hover > a, .entry-link h4, a, .comingsoon .social-share ul li a:hover, .format-link .entry-header, 
		#comingsoon-countdown .countdown-period, .widget .nav > li.active > a, .widget .nav > li:hover > a, .widget.widget_mc4wp_widget .button i, 
		.navbar-main .dropdown-menu>li>a:hover, .navbar-main .dropdown-menu>li>a:focus, .widget.widget_mc4wp_widget h3:before, 
		.widget .tagcloud a:hover, .widget caption, .widget thead th, .footer-menu >li >a:hover,.woocommerce-page #content div.product p.price{ color: #62a83d; }.progress-bar, input[type="submit"], .single-post .post-navigation, .widget .nav, .book_wrapper a.btn-download, .btn-commom, .profile-tab ul.nav-tabs>li>a:hover, .profile-tab ul.nav-tabs>li.active>a, .profile-tab ul.nav-tabs>li.active>a:hover, .profile-tab ul.nav-tabs>li.active>a:focus, .pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus, .pagination>li>a:hover, .pagination>li>span:hover, .pagination>li>a:focus, .pagination>li>span:focus, .single-post .post-navigation .post-controller .previous-post a, .single-post .post-navigation .post-controller .next-post a, .post-content.media .post-format i, #searchform .btn-search, #blog-gallery-slider .carousel-control.left, #blog-gallery-slider .carousel-control.right{ background: #62a83d; }a:hover,.post-content.media h2.entry-title a:hover,.review-item-text .entry-title a:hover{ color: #7BC256; }.wpb_wrapper .wpb_accordion .wpb_accordion_wrapper .ui-state-active .ui-icon,.btn-commom:hover,.product-thumbnail-outer .addtocart-btn:before, a.load-more:hover,.featured .pricing-plan .plan-title{ background-color: #7BC256 !important; }.header{ background: #62a83d; }#masthead.sticky{ background-color: rgba(98,168,61,.85); }#masthead.sticky{ position:fixed; z-index:99999;margin:0 auto 30px; width:100%;}#masthead.sticky .navbar.navbar-default{ background: rgba(255,255,255,.95); border-bottom:1px solid #f5f5f5}header.sticky {
	  position: fixed;
	  top: -165px;
	  left: 0;
	  right: 0;
	  z-index: 999;
	  -webkit-transition: 0.2s top cubic-bezier(.3,.73,.3,.74);
	  -moz-transition: 0.2s top cubic-bezier(.3,.73,.3,.74);
	  -ms-transition: 0.2s top cubic-bezier(.3,.73,.3,.74);
	  -o-transition: 0.2s top cubic-bezier(.3,.73,.3,.74);
	  transition: 0.2s top cubic-bezier(.3,.73,.3,.74);
	}
	body.down header.sticky { top: 0; }body.down.admin-bar header.sticky{ top: 32px; }.header{ margin-top: 0px; }.header{ margin-bottom: 30px; }#footer{ background: #62A83D; }
	</style>
</head>

 <?php 

     if ( isset($themeum_options['boxfull-en']) ) {
      $layout = $themeum_options['boxfull-en'];
     }else{
        $layout = 'fullwidth';
     }
 ?>

<body <?php body_class( $layout.'-bg' ); ?>>

<?php
    if( $themeum_options['hide-cart'] && $themeum_options['hide-saerch'] ){
        $ol_menu_class = 'col-sm-8';
    }else{
        $ol_menu_class = 'col-sm-8';
    }
 ?>

    
	<div id="page" class="hfeed site <?php echo $layout; ?>" >
		<header id="masthead" class="site-header header" role="banner">
        <div class="home-search">
        <div class="container"><?php echo get_search_form();?></div>
        <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
        </div>
			<div id="header-container" class="container">
				<div id="navigation">
                    <div class="row">

                        <div class="col-sm-3">
        					<div class="navbar-header">
        						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        							<span class="icon-bar"></span>
        							<span class="icon-bar"></span>
        							<span class="icon-bar"></span>
        						</button>
        	                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
        	                    	<h1 class="logo-wrapper">
        		                    	<?php
        									if (isset($themeum_options['logo']))
        								   {
        								   		
        										if($themeum_options['logo-text-en']) {
        											echo $themeum_options['logo-text'];
        										}
        										else
        										{
        											if(!empty($themeum_options['logo'])) {
        											?>
        												<img class="enter-logo img-responsive" src="<?php echo $themeum_options['logo']['url']; ?>" alt="" title="">
        											<?php
        											}else{
        												echo get_bloginfo('name');
        											}
        										}
        								   }
        									else
        								   {
        								    	echo get_bloginfo('name');
        								   }
        								?>
        		                    </h1>
        		                </a>
        					</div>
                        </div>

                        <div id="main-menu" class="<?php echo $ol_menu_class; ?> hidden-xs">
                            <?php 
                            if ( has_nav_menu( 'primary' ) ) {
                                wp_nav_menu(  array(
                                    'theme_location' => 'primary',
                                    'container'      => '', 
                                    'menu_class'     => 'nav',
                                    'fallback_cb'    => 'wp_page_menu',
                                    'depth'          => 3,
                                    'walker'         => new Megamenu_Walker()
                                    )
                                ); 
                            }
                            ?>
                            
                        </div><!--/#main-menu-->
                        
                        <?php if( isset($themeum_options['hide-saerch']) || isset($themeum_options['hide-cart']) ){ ?>  
                        <div class="col-sm-1 cart-busket">
                           
                            <?php if(isset($themeum_options['hide-cart']) && !$themeum_options['hide-cart']){ ?>
                                <?php if($woocommerce) { ?>
                                <div class="woo-cart">
                                    <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>">
                                        <?php
                                            $has_products = '';
                                            if($woocommerce->cart->cart_contents_count) {
                                                $has_products = 'cart-has-products';
                                            }
                                        ?>
                                        <span class="woo-cart-items">
                                            <span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                                        </span>
                                        <i class="fa fa-shopping-cart"></i>
                                    </a>
                                    <?php the_widget( 'WC_Widget_Cart', 'title= ' ); ?>
                                </div>
                                <?php } ?>
                            <?php } ?>

                            <?php if(isset($themeum_options['hide-saerch']) && !$themeum_options['hide-saerch']){ ?>
                            <span class="home-search-btn">
                                <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                            </span>
                            <?php }  ?>
                        </div>
                        <?php } ?>
                            
                        <div id="mobile-menu" class="visible-xs">
                            <div class="collapse navbar-collapse">
                                <?php 
                                if ( has_nav_menu( 'primary' ) ) {
                                    wp_nav_menu( array(
                                        'theme_location'      => 'primary',
                                        'container'           => false,
                                        'menu_class'          => 'nav navbar-nav',
                                        'fallback_cb'         => 'wp_page_menu',
                                        'depth'               => 3,
                                        'walker'              => new wp_bootstrap_mobile_navwalker()
                                        )
                                    ); 
                                }
                                ?>
                            </div>
                        </div><!--/.#mobile-menu-->
                    </div><!--/.row--> 
				</div><!--/.container--> 
			</div>
		</header><!--/#header-->

