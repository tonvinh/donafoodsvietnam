<?php
add_shortcode( 'themeum_list_category_item', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'list' => 'list-default',
		'color' => '#62A83D',
		'size' => '14',
		'input' => '',
		'desc' => '',
		'class' => '',
                'category' => '',
                'number' => '',
		), $atts));

	$style = '';

	$font_size = '';

	if($color) $style .= 'color:' . $color  . ';';

	if($size) $style .= 'font-size:' . (int) $size . 'px;';

	$output = '';

	$output .= '<div class="list '.$list.' " style="'.$style.'">';
        
         $args = array(
            'post_type' => 'post',
            'post_per_page' => $number,            
            'category' => $category
        );
        
        $data = get_posts($args);
        if(!empty($data)) {
            $input = '<ul>';
            foreach($data as $item) {
                $input .= '<li><a href="'.get_permalink($item->ID).'">'.$item->post_title.'</a></li>';
            }
            $input .= "</ul>";
        }      

	$output .= $input;
	
	$output .= '</div>';

	return $output;

});


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
    
        $args = array(
            'hide_empty'       => 0,
            'hierarchical'     => true,
            'orderby'          => 'name',
            'order' => 'ASC'
            );
        $list_category =  get_categories($args);
        
        if(!empty($list_category)) {
            $arr_cat = array();
            foreach($list_category as $item) {
                $arr_cat[$item->name] = $item->term_id;
            }
        }
        
	vc_map(array(
		"name" => __("Themeum List Category Item", "themeum"),
		"base" => "themeum_list_category_item",
		'icon' => 'icon-thm-list-category-item',
		"category" => __('Themeum', "themeum"),
		"params" => array(
                        array(
				"type" => "dropdown",
				"heading" => __("Category", "themeum"),
				"param_name" => "category",
				"value" => $arr_cat,
				),
                    
                       array(
				"type" => "textfield",
				"heading" => __("Number", "themeum"),
				"param_name" => "number",
				"value" => "",
				),
			array(
				"type" => "dropdown",
				"heading" => __("List Item", "themeum"),
				"param_name" => "list",
				"value" => array('Default'=>'list-default','Number'=>'list-number','Square'=>'list-square','Number 2'=>'list-number-background','List Arrow'=>'list-arrow','List Circle'=>'list-circle','List Star'=>'list-star'),
				),				
				

			array(
				"type" => "colorpicker",
				"heading" => __("Font Color", "themeum"),
				"param_name" => "color",
				"value" => "",
				),	
				

			array(
				"type" => "textfield",
				"heading" => __("Font Size", "themeum"),
				"param_name" => "size",
				"value" => "",
				),		
//
//			array(
//				"type" => "textarea_html",
//				"heading" => __("Input List", "themeum"),
//				"param_name" => "input",
//				"value" => "",
//				),	


			array(
				"type" => "textfield",
				"heading" => __("Custom Class ", "themeum"),
				"param_name" => "class",
				"value" => "",
				)


			),
		));
}
?>